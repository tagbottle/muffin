# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased](https://gitlab.com/tagbottle/muffin/compare/0.0.0...HEAD) 

## [0.0.0](https://gitlab.com/tagbottle/muffin/compare/0.0.0...0.0.0) - 2018-08-23
### Added
- CHANGELOG.md
